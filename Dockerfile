# Scaryland Back End Builder
FROM node:16.13-alpine3.12 as scarylandBackBuilder
RUN mkdir -p /home/node/scaryland/node_modules && chown -R node:node /home/node/scaryland
WORKDIR /home/node/scaryland
COPY package*.json ./
USER node
RUN npm install
COPY --chown=node:node . .
RUN npm run build-prod

# Scaryland Prod
FROM node:16.13-alpine3.12
RUN mkdir -p /usr/local/scaryland && chown -R node:node /usr/local/scaryland
WORKDIR /usr/local/scaryland
COPY package*.json ./
USER node
RUN npm install --production
COPY --from=scarylandBackBuilder /home/node/scaryland/dist ./dist

ARG PORT=8080
ARG DB_NAME=players_db
ARG DB_HOST=cluster0.hjz7e.mongodb.net
ARG DB_USER=playersDbUser
ARG DB_PASSWORD=CfDAXs3R8gXMdwN

ENV ENV "production"
ENV PORT ${PORT}
ENV DB_NAME ${DB_NAME}
ENV DB_HOST ${DB_HOST}
ENV DB_USER ${DB_USER}
ENV DB_PASSWORD ${DB_PASSWORD}

CMD ["npm", "run", "production"]