# Backend server for ScaryLand

To run the project you can use the command:

> npm start

There are a **_.env.example_** file in the root, so you must to copy that file and rename it to **_.env_**. Then in that file, you can configure the sensible data of the project, like the database connection, or the port of the server.
