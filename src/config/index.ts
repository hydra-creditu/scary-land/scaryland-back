import dotenv from "dotenv";

dotenv.config();

const { ENV, PORT, DB_NAME, DB_HOST, DB_USER, DB_PASSWORD } = process.env;

export default {
  env: {
    name: ENV,
    port: PORT,
  },
  db: {
    name: DB_NAME,
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
  },
};
