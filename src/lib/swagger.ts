export const swaggerDocs = {
  swagger: "2.0",
  info: {
    description:
      "This is the documentation for the ScaryLand Game console. Here you will find the endpoints that are used in the dashboard of players.",
    version: "0.0.1",
    title: "ScaryLand API",
    termsOfService: "",
    contact: {
      email: "hydrasoftco@gmail.com",
    },
    license: {
      name: "Apache 2.0",
      url: "https://www.apache.org/licenses/LICENSE-2.0",
    },
  },
  host: "api.scaryland.xyz",
  basePath: "/api/v1",
  tags: [
    {
      name: "players",
      description: "Everything about players",
    },
    {
      name: "users",
      description: "Everything about users info",
    },
  ],
  schemes: ["https"],
  paths: {
    "/hall-fame": {
      get: {
        tags: ["players"],
        summary: "Get all the players of the hall of fame",
        description: "",
        operationId: "getPlayers",
        consumes: ["application/json"],
        produces: ["application/json"],
        parameters: [
          {
            in: "query",
            name: "status",
            type: "string",
            description: "Filter of players",
            required: false,
          },
        ],
        responses: {
          "200": {
            description: "Successful operation",
            schema: {
              $ref: "#/definitions/ResponseHallFame",
            },
          },
        },
      },
    },
    "/generic/avatars": {
      get: {
        tags: ["generic"],
        summary: "Get list of avatars",
        description: "",
        operationId: "getAvatars",
        consumes: ["application/json"],
        produces: ["application/json"],
        responses: {
          "200": {
            description: "Successful operation",
            schema: {
              $ref: "#/definitions/ResponseAvatars",
            },
          },
        },
      },
    },
    "/generic/levels": {
      get: {
        tags: ["generic"],
        summary: "Get list of Status/Levels",
        description: "",
        operationId: "getLevels",
        consumes: ["application/json"],
        produces: ["application/json"],
        responses: {
          "200": {
            description: "Successful operation",
            schema: {
              $ref: "#/definitions/ResponseLevels",
            },
          },
        },
      },
    },
  },
  securityDefinitions: {
    petstore_auth: {
      type: "oauth2",
      authorizationUrl: "http://petstore.swagger.io/oauth/dialog",
      flow: "implicit",
      scopes: {
        "write:pets": "modify pets in your account",
        "read:pets": "read your pets",
      },
    },
  },
  definitions: {
    Level: {
      type: "object",
      properties: {
        uid: {
          type: "string",
        },
        nameStatus: {
          type: "string",
        },
        minScore: {
          type: "string",
        },
        maxScore: {
          type: "string",
        },
        logo: {
          type: "string",
        },
      },
    },
    Avatar: {
      type: "object",
      properties: {
        uid: {
          type: "string",
        },
        path: {
          type: "string",
        },
      },
    },
    Player: {
      type: "object",
      properties: {
        id: {
          type: "string",
        },
        nickName: {
          type: "string",
        },
        avatar: {
          $ref: "#/definitions/Avatar",
        },
        statistics: {
          type: "object",
          properties: {
            bestScore: {
              type: "string",
            },
            bestTime: {
              type: "string",
            },
            bestRanking: {
              type: "string",
            },
            currentRanking: {
              type: "string",
            },
          },
        },
        status: {
          type: "boolean",
          default: true,
        },
        createdAt: {
          type: "string",
          format: "date-time",
        },
        updatedAt: {
          type: "string",
          format: "date-time",
        },
      },
    },
    ResponseHallFame: {
      type: "object",
      properties: {
        code: {
          type: "integer",
          format: "int32",
        },
        error: {
          type: "string",
        },
        body: {
          type: "array",
          items: {
            $ref: "#/definitions/Player",
          },
        },
      },
    },
    ResponseAvatars: {
      type: "object",
      properties: {
        code: {
          type: "integer",
          format: "int32",
        },
        error: {
          type: "string",
        },
        body: {
          type: "array",
          items: {
            $ref: "#/definitions/Avatar",
          },
        },
      },
    },
    ResponseLevels: {
      type: "object",
      properties: {
        code: {
          type: "integer",
          format: "int32",
        },
        error: {
          type: "string",
        },
        body: {
          type: "array",
          items: {
            $ref: "#/definitions/Levels",
          },
        },
      },
    },
  },
  externalDocs: {
    description: "Find out more about Swagger",
    url: "http://swagger.io",
  },
};
