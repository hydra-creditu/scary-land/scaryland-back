import mongoose from "mongoose";
import config from "../config";

let connection: any;

const mongoUri = `mongodb+srv://${config.db.user}:${config.db.password}@${config.db.host}/${config.db.name}`;

async function connectDB() {
  if (connection) return connection;
  try {
    connection = await mongoose.connect(mongoUri);
  } catch (e) {
    console.error(`Couldn't connect to db`, mongoUri, e);
    throw new Error("Internal Server Error");
  }
  return connection;
}

export { connectDB };
