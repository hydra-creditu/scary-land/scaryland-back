import mongoose from "mongoose";

const { Schema } = mongoose;

const Player = new Schema({
  id: String,
  nickName: String,
  avatar: {
    type: Schema.Types.ObjectId,
    ref: "avatars",
  },
  statistics: {
    bestScore: String,
    bestTime: String,
    bestRanking: String,
    currentRanking: {
      type: Number,
    },
    currentStatus: String,
  },
  createdAt: Date,
  updatedAt: Date,
  status: Boolean,
});

const model: mongoose.Model<
  mongoose.Document<any, any, any>,
  any,
  any
> = mongoose.model("players", Player);

export default model;
