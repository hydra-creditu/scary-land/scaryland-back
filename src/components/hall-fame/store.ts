import PlayersModel from "./model";

const queryGetPlayers = async (
  filter: { [key: string]: any } | null = null
) => {
  const sort = {
    sort: {
      "statistics.currentRanking": 1,
    },
  };
  if (filter) {
    return await PlayersModel.find(filter, { _id: 0 }, sort).populate("avatar");
  } else {
    return await PlayersModel.find(
      {},
      { _id: 0 },
      { ...sort, limit: 10 }
    ).populate("avatar");
  }
};

export { queryGetPlayers };
