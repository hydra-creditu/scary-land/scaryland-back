import { getPlayersList } from "./controller";
import { Request, Response, Router } from "express";
import { connectDB } from "../../lib/db";
import { success, error } from "../../network/response";

const router = Router();

router.get("/", async (req: Request, res: Response): Promise<void> => {
  try {
    await connectDB();
    const {
      query: { filter },
    } = req;
    const listPlayers = await getPlayersList(
      filter ? { filter: filter?.toString() } : null
    );
    success(req, res, listPlayers, 200);
  } catch (errorMsg: any) {
    error(req, res, errorMsg.message || errorMsg, 500);
  }
});

export default router;
