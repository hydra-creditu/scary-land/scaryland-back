import { queryGetPlayers } from "./store";

const getPlayersList = async (
  filter: {
    [key: string]: string;
  } | null
): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      let playersList;
      if (filter) {
        playersList = await queryGetPlayers({ id: filter.filter });
        if (playersList.length === 0) {
          playersList = queryGetPlayers({
            $or: [
              {
                nickName: new RegExp(filter.filter),
              },
              {
                "statistics.currentStatus": new RegExp(filter.filter),
              },
            ],
          });
        }
      } else {
        playersList = queryGetPlayers();
      }
      resolve(playersList);
    } catch (error) {
      reject(error);
    }
  });
};

export { getPlayersList };
