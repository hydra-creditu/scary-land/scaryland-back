import { Request, Response, Router } from "express";
import { success } from "../../network/response";

const router = Router();

router.get("/", async (req: Request, res: Response): Promise<void> => {
  success(req, res, {}, 204);
});

export default router;
