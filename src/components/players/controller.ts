import { queryGetPlayerInfo } from "./store";

const getPlayerInfo = async (filter: string): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      const playersList = await queryGetPlayerInfo(filter);
      resolve(playersList);
    } catch (error) {
      reject(error);
    }
  });
};

export { getPlayerInfo };
