import PlayersModel from "./model";

const queryGetPlayerInfo = async (filter?: string) => {
  const sort = {
    sort: {
      "statistics.currentRanking": 1,
    },
  };
  if (filter) {
    return await PlayersModel.find({ id: filter }, { _id: 0 }, sort);
  }
};

export { queryGetPlayerInfo };
