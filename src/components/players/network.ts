import { Request, Response, Router } from "express";
import { connectDB } from "../../lib/db";
import { error, success } from "../../network/response";
import { getPlayerInfo } from "./controller";

const router = Router();

router.get("/:idPlayer", async (req: Request, res: Response): Promise<void> => {
  try {
    await connectDB();
    const listPlayers = await getPlayerInfo(req.params.idPlayer);
    success(req, res, listPlayers, 200);
  } catch (errorMsg: any) {
    error(req, res, errorMsg.message || errorMsg, 500);
  }
});

export default router;
