import { Request, Response, Router } from "express";
import { connectDB } from "../../lib/db";
import { error, success } from "../../network/response";
import { getAvatarList, getLevelsList } from "./controller";

const router = Router();

router.get("/levels", async (req: Request, res: Response): Promise<void> => {
  try {
    await connectDB();
    const listLevels = await getLevelsList();
    success(req, res, listLevels, 200);
  } catch (errorMsg) {
    error(req, res, errorMsg, 400);
  }
});

router.get("/avatars", async (req: Request, res: Response): Promise<void> => {
  try {
    await connectDB();
    const avatarList = await getAvatarList();
    success(req, res, avatarList, 200);
  } catch (errorMsg) {
    error(req, res, errorMsg, 400);
  }
});

export default router;
