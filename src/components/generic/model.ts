import mongoose from "mongoose";

const { Schema } = mongoose;

const LevelSchema = new Schema({
  uid: { type: String, unique: true },
  nameStatus: String,
  minScore: Number,
  maxScore: Number,
  logo: String,
});

const AvatarSchema = new Schema({
  uid: { type: String, unique: true },
  path: String,
});

const LevelModel: mongoose.Model<
  mongoose.Document<any, any, any>,
  any,
  any
> = mongoose.model("avatars", AvatarSchema);

const AvatarModel: mongoose.Model<
  mongoose.Document<any, any, any>,
  any,
  any
> = mongoose.model("levels", LevelSchema);

export { LevelModel, AvatarModel };
