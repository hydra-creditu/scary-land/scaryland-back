import { queryGetAvatars, queryGetLevels } from "./store";

const getLevelsList = async (): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      const levelsList = await queryGetLevels();
      resolve(levelsList);
    } catch (error) {
      reject(error);
    }
  });
};

const getAvatarList = async (): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      const avatarList = await queryGetAvatars();
      resolve(avatarList);
    } catch (error) {
      reject(error);
    }
  });
};

export { getLevelsList, getAvatarList };
