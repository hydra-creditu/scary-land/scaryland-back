import { AvatarModel, LevelModel } from "./model";

const queryGetLevels = async () => {
  return await LevelModel.find({}, { _id: 0 });
};

const queryGetAvatars = async () => {
  return await AvatarModel.find({}, { _id: 0 });
};

export { queryGetLevels, queryGetAvatars };
