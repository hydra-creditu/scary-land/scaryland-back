import cors from "cors";
import express, { Router } from "express";
import morgan from "morgan";
import routes from "./network/routes";
import config from "./config/index";
import helmet from "helmet";

const router = Router();

const app = express();

app.set("json spaces", 2);
app.use(cors());
app.use(helmet());
app.use(morgan("combined"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/uploads", express.static("uploads"));

routes(router);

app.use("/api/v1", router);

app.listen(config.env.port, () => {
  console.log(`Server on port ${config.env.port}`);
});
