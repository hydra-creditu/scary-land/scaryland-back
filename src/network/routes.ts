import { Router } from "express";
import HallFameRouter from "../components/hall-fame/network";
import GenericRouter from "../components/generic/network";
import AvailabilityRouter from "../components/availabilty/network";
import swaggerUi from "swagger-ui-express";
import { swaggerDocs } from "../lib/swagger";

const routes = (server: Router): void => {
  server.use("/ping", AvailabilityRouter);
  server.use("/hall-fame", HallFameRouter);
  server.use("/generic", GenericRouter);
  server.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
};

export default routes;
