import { Request, Response } from "express";

const success = (
  req: Request,
  res: Response,
  data: any,
  code: number,
  details?: any
) => {
  res.status(code).send({
    code,
    error: null,
    count: data.length || 0,
    body: data,
  });
};

const error = (
  req: Request,
  res: Response,
  data: any,
  code: number,
  details?: any
): void => {
  console.error(`[ErrorResponse] ${details}`);
  res.status(code).send({ error: data, body: null });
};

export { success, error };
